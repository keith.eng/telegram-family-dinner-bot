export const creationStepOneTemplate =  `Are you looking to "host" or to be "hosted" or "both" ?`;

export const creationStepTwoTemplate =
`Add a small description roughly 2-3 sentences about why you want to host/be hosted and maybe some details about yourself like occupation 🚜 and hobbies 🏀 and whatnot! Heres a sample:

"My name is Keith! I'm a 25 year old Software Engineer. Have been living alone for the last 2 years and I really miss a hearty homecooked meal. I like to work on fun coding projects in my free time and am learning how to cook!"

=================
Type Below:
=================
`;
