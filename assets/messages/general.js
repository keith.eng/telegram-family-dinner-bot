export const unknownTemplate =
`❌ Unknown Command!

Reason(s):
ℹ️ You might have sent message(s) to the bot when not prompted to.
ℹ️ You might have sent the wrong message type.
ℹ️ The bot structure could have been been updated.

✅ Only send messages to the bot when prompted.
✅ Follow the instructions given by the bot.
✅ Tap on /menu to refresh the bot.`; 

export const menuTemplate = (username) => {
return `Hello ${username},
Welcome to the Singapore's MealShare Bot!🤖
=======================
What can I help you to do?
=======================
1️⃣ [/viewprofile] : View your profile👨🏻‍💼
2️⃣ [/editprofile] : Edit your profile✏️
3️⃣ [/match] : Match with a host/hostee!🔥
=======================
Please read the [/userguide] for more information.`
}

export const userguide = (username) => {
return `Hello ${username},
Welcome to the Singapore's MealShare Bot!🤖
=======================
This bot is created by @keithbeef. Conceptually, it works like Homestay but for a meal! 🥘 Register as either a host or a hostee, fill out some simple baseline descriptions, and get matched with other interested parties!

Chat with each other and invite them over to break bread! All messages are monitored and offending parties will be reported to the police and blacklisted. 👮 
=======================
⚠️ At any point if the bot is stuck, type /menu to refresh the bot or contact /support for live chat support.
=======================`
}

export const botError = `Something went wrong with the bot! 🤖💀 Send /menu to restart!`;
