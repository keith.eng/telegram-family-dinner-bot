export const viewProfile = (profile) => {
return `Status: ${profile.status === 0 ? "Seeking to host 🏠" : profile.status === 1 ? "Looking to be hosted 🍲" : profile.status === 2 ? "Looking to either host or be hosted 🥡" : "Unset! Please set. 😊"}
Description: ${profile?.description || "Unset! 😊"}
=======================
Please read the [/userguide] for more information.
=======================
⚠️ At any point if the bot is stuck, type /menu to refresh the bot or contact /support for live chat support.`
};

export const editProfile =
`What would you like to edit! 📝
/status, /description
=======================
Please read the [/userguide] for more information.
=======================
⚠️ At any point if the bot is stuck, type /menu to refresh the bot or contact /support for live chat support.`;

export const noProfileFound =
`No profile found associated with this user! 😥
Type /start to create your profile!
=======================
Please read the [/userguide] for more information.
=======================
⚠️ At any point if the bot is stuck, type /menu to refresh the bot or contact /support for live chat support.`;

export const isMatching = `You are now Matching! ✨ Please hold on while we find a suitable match for you.`;
export const cancelMatch = `If you want to cancel matching, just type /cancel! ❌`;
export const cancellingMatch = `Cancelling match.... ⏳`;
export const cancelledMatch = `Matching Cancelled! ❌`;
