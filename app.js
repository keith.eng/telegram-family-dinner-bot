import botAPI from "telegram-bot-api";

import { unknownTemplate,
  menuTemplate,
  userguide,
  botError
} from "#assets/messages/general.js";

import {
  viewProfile,
  editProfile,
  noProfileFound,
  isMatching,
  cancelMatch,
  cancellingMatch,
  cancelledMatch
} from "#assets/messages/profile.js";

import {
  creationStepOneTemplate, 
  creationStepTwoTemplate, 
} from "#assets/messages/registration.js";

import { insertUser, selectUser, updateEditingStage, updateMatch } from "#controllers/user.js";
import { registrationStepOne, registrationStepTwo } from "#controllers/register.js";

const api = new botAPI({
  token: process.env.BOT_TOKEN,
});

api.getMe().then(console.log).catch(console.err);

const mp = new botAPI.GetUpdateMessageProvider();
api.setMessageProvider(mp);
api.start().then(console.log("API is started")).catch(console.err);

api.on("update", (update) => {
  const chat_id = update.message?.chat?.id || update.callback_query?.from?.id;
  const message = update.message?.text || update.callback_query?.data;
  const username = update?.message?.from?.username || update.callback_query?.from?.username;

  try {
    let data = {
      chat_id: chat_id,
    };
    if (chat_id && message && username) {
      switch(message) {
        case "/start":
          selectUser({ user_id: chat_id }).then(user => {
            if (user.length === 0) {
              insertUser({ user_id: chat_id }).then(creationSuccess => {
                if (creationSuccess) {
                  api.sendMessage({
                    text: userguide(username),
                    ...data
                  }).then(
                    api.sendMessage({
                      text: creationStepOneTemplate,
                      reply_markup: {
                        inline_keyboard: [
                          [{
                              "text": "Host",
                              "callback_data": "host"            
                          }, 
                          {
                            "text": "Hosted",
                            "callback_data": "hosted"            
                          }], 
                          [{
                            "text": "Both",
                            "callback_data": "both"            
                          }]
                        ]
                      },
                      ...data
                    })
                  );
                  return;
                };
                api.sendMessage({
                  text: menuTemplate(username),
                  ...data
                })  
              });
              return;
            }
            api.sendMessage({
              text: menuTemplate(username),
              reply_markup: {
                inline_keyboard: [
                  [{
                      "text": "View Profile",
                      "callback_data": "/viewprofile"            
                  }, 
                  {
                    "text": "Edit Profile",
                    "callback_data": "/editprofile"            
                  }], 
                  [{
                    "text": "Match",
                    "callback_data": "/match"            
                  }]
                ]
              },
              ...data
            });
          });
          break;
        case "/menu":
          api.sendMessage({
            text: menuTemplate(username),
            reply_markup: {
              inline_keyboard: [
                [{
                    "text": "View Profile",
                    "callback_data": "/viewprofile"            
                }, 
                {
                  "text": "Edit Profile",
                  "callback_data": "/editprofile"            
                }], 
                [{
                  "text": "Match",
                  "callback_data": "/match"            
                }]
              ]
            },
            ...data
          });
          break;
        case "/viewprofile":
          selectUser({ user_id: chat_id }).then(user => {
            if (user.length === 0) {
              api.sendMessage({
                text: noProfileFound,
                ...data
              });
              return;
            }
            api.sendMessage({
              text: viewProfile(user[0]),
              ...data
            });
          });
          break;
        case "/editprofile":
          api.sendMessage({
            text: editProfile,
            reply_markup: {
              inline_keyboard: [
                [{
                    "text": "Status",
                    "callback_data": "/status"            
                }, 
                {
                  "text": "Description",
                  "callback_data": "/description"            
                }],
              ]
            },
            ...data
          });
          break;
        case "/status":
          updateEditingStage(chat_id, 0, 1).then(result => {
            if (result) {
              api.sendMessage({
                text: creationStepOneTemplate,
                reply_markup: {
                  inline_keyboard: [
                    [{
                        "text": "Host",
                        "callback_data": "host"            
                    }, 
                    {
                      "text": "Hosted",
                      "callback_data": "hosted"            
                    }], 
                    [{
                      "text": "Both",
                      "callback_data": "both"            
                    }]
                  ]
                },
                ...data
              })
              return
            }
            api.sendMessage({
              text: botError,
              ...data
            });
          })
          break;
        case "/description":
          updateEditingStage(chat_id, 1, 1).then(result => {
            if (result) {
              api.sendMessage({
                text: creationStepTwoTemplate,
                ...data
              })
              return
            }
            api.sendMessage({
              text: botError,
              ...data
            });
          })
          break;
        case "/userguide":
          api.sendMessage({
            text: userguide(username),
            ...data
          });
          break;
        case "/match":
          updateMatch(chat_id, 1).then(result => {
            if (result) {
              api.sendMessage({
                text: isMatching,
                ...data
              });
              api.sendMessage({
                text: cancelMatch,
                ...data
              })
              return
            }
            api.sendMessage({
              text: botError,
              ...data
            });
          })
          break;
        case "/cancel":
          api.sendMessage({
            text: cancellingMatch,
            ...data
          });
          updateMatch(chat_id, 0).then(result => {
            if (result) {
              api.sendMessage({
                text: cancelledMatch,
                ...data
              });
              return
            }
            api.sendMessage({
              text: botError,
              ...data
            });
          })
          break;
        default:
          selectUser({ user_id: chat_id }).then(user => {
            if (user.length === 0) {
              api.sendMessage({
                text: unknownTemplate,
                ...data
              });
              return;
            }
            const stage = user[0]?.registration_stage;
            const editing_mode = user[0]?.editing_mode;
            switch(stage) {
              case 0:
                registrationStepOne(chat_id, message).then(result => {
                  if(result && editing_mode) {
                    updateEditingStage(chat_id, -1, 0);
                    selectUser({ user_id: chat_id }).then(user => {
                      if (user.length === 0) {
                        api.sendMessage({
                          text: noProfileFound,
                          ...data
                        });
                        return;
                      }
                      api.sendMessage({
                        text: viewProfile(user[0]),
                        ...data
                      });
                    });
                    return;
                  } else if (result) {
                    api.sendMessage({
                      text: creationStepTwoTemplate,
                      ...data
                    })
                    return;
                  }                  
                  api.sendMessage({
                    text: unknownTemplate,
                    ...data
                  });
                })
                break;
              case 1:
                registrationStepTwo(chat_id, message).then(result => {
                  if(result && editing_mode) {
                    updateEditingStage(chat_id, -1, 0);
                    selectUser({ user_id: chat_id }).then(user => {
                      if (user.length === 0) {
                        api.sendMessage({
                          text: noProfileFound,
                          ...data
                        });
                        return;
                      }
                      api.sendMessage({
                        text: viewProfile(user[0]),
                        ...data
                      });
                    });
                    return;
                  } else if (result) {
                    api.sendMessage({
                      text: menuTemplate(username),
                      ...data
                    })
                    return;
                  }   
                  api.sendMessage({
                    text: unknownTemplate,
                    ...data
                  })  
                })
                break;
              default:
                api.sendMessage({
                  text: unknownTemplate,
                  ...data
                });
                break;
            }
          });
          break;
      }
      return
    }
    api.sendMessage({
      text: botError,
      ...data
    });
    return
  } catch (error) {
    console.error(error);
    api.sendMessage({
      text: botError,
      ...data
    });
    return;
  }
});
