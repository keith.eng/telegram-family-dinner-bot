import { client } from "#config/database.js";

export async function registrationStepOne(user_id, input) {
    await client.connect();
    const database = client.db(process.env.MONGO_DB);
    let status;
    if (input === "host") {
        status = 0;
    } else if (input === "hosted") {
        status = 1;
    } else if (input === "both") {
        status = 2;
    } else { return false }

    try {
        return await database.collection("users").updateOne({ user_id }, { $set: { status, registration_stage: 1 } })
            .then(result => result.modifiedCount == 1 && true)
            .catch((err) => {
                console.error(`Failed to update user: ${err}`);
                return false;
            });
    } catch (err) {
        throw err;
    } finally {
        await client.close();
    }
}

export async function registrationStepTwo(user_id, description) {
    await client.connect();
    const database = client.db(process.env.MONGO_DB);

    try {
        return await database.collection("users").updateOne({ user_id }, { $set: { description, registration_stage: -1 } })
            .then(result => result.modifiedCount == 1 && true)
            .catch((err) => {
                console.error(`Failed to update user: ${err}`);
                return false;
            });
    } catch (err) {
        throw err;
    } finally {
        await client.close();
    }
}
