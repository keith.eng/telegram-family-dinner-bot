import { client } from "#config/database.js";

export async function insertUser(userObj) {
  await client.connect();
  const database = client.db(process.env.MONGO_DB);

  try {
    return await database.collection("users").insertOne({ registration_stage: 0, ...userObj })
    .then(() => {
      return true;
    }).catch((err) => {
      console.error(`Failed to insert users: ${err}`);
      return false;
    });
  } catch (err) {
    throw err;
  } finally {
    await client.close();
  }
}

export async function selectUser(userQuery) {
  await client.connect();
  const database = client.db(process.env.MONGO_DB);

  try {
    return await database
      .collection("users")
      .find(userQuery)
      .toArray()
      .then((items) => {
        return items;
      })
      .catch((err) => {
        console.error(`Failed to find users: ${err}`);
        return [];
      });
  } catch (err) {
    throw err;
  } finally {
    await client.close();
  }
}

export async function updateEditingStage(user_id, registration_stage, editing_mode) {
  await client.connect();
  const database = client.db(process.env.MONGO_DB);

  try {
    return await database.collection("users").updateOne({ user_id }, { $set: { registration_stage, editing_mode } })
      .then(result => result.modifiedCount == 1 && true)
      .catch((err) => {
          console.error(`Failed to update user: ${err}`);
          return false;
      });
  } catch (err) {
      throw err;
  } finally {
      await client.close();
  }
}

export async function updateMatch(user_id, isMatching) {
  await client.connect();
  const database = client.db(process.env.MONGO_DB);

  try {
    return await database.collection("users").updateOne({ user_id }, { $set: { is_matching: isMatching } })
      .then(result => result.modifiedCount == 1 && true)
      .catch((err) => {
          console.error(`Failed to match user: ${err}`);
          return false;
      });
  } catch (err) {
      throw err;
  } finally {
      await client.close();
  }
}
