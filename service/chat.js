import { client } from "#config/database.js";

export async function initializeNewUser(userObj) {
  await client.connect();
  const database = client.db(process.env.MONGO_DB);

  try {
    await database.collection("users").insertOne(userObj);
  } catch (err) {
    throw err;
  } finally {
    await client.close();
  }
}
